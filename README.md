# MicroZuul

L'objectif de ce TP était de créer une application multiservices avec l'aide de ZUUL.

Nous avons réussi à créer une redirection zuul, que nous lançons depuis une application springboot, sur l'adresse http://localhost:8089/patient/{id}

Cela nous renvoie un patient créé avec comme valeurs
{"id":id rentré,"name":"NameTest" + id rentré,"surname":"SurnameTest" + id rentré,"age":29 + id rentré}

Nous avons aussi réussi à créer un docker-compose.yml qui lance une bdd postgres, et une instance de EAI mirth connect

Pour la liaison entre ces applications, nous n'avons pas réussi à la mettre en place car nous avons rencontré beaucoup de prolèmes, déjà lors de la mise en place de la base de données, mais aussi pour ce qui est accès depuis zuul. 

L'utilisation de la bibliothèque HL7 n'était pas simple non plus, chose pour laquelle nous avons échoué, c'est pourquoi mirth n'est pas relié à zuul, et qu'il est juste dans le docker-compose.yml

Nous avons donc dans notre projet : une application springboot, qui utilise zuul pour ses routes, et un docker-compose.yml qui lance une instance postgres et une instance mirth connect