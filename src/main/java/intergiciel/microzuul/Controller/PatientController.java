package intergiciel.microzuul.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import intergiciel.microzuul.Metier.Patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
public class PatientController {

    @GetMapping("/patient/{id}")
    public Patient addNewPatient(@PathVariable long id, HttpServletRequest req, HttpServletResponse res) {
        if (req.getHeader("Test") != null) {
            res.addHeader("Test", req.getHeader("Test"));
        }
        return new Patient(id, "NameTest"+id, "SurnameTest" + id, (int) (29+id));
    }
}
